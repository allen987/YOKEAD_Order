# YOKEAD_Order

#### 项目介绍
广告相关 的可以控制nginx 以及下订单 使用java8 开发（代码里有部分lambda表达式）

页面基于layui 样式编写


#### 软件架构
1. java 操作动态解析并修改nginx配置
2. 订单使用mysql 数据库
3. 使用七牛云存储图片
4. 使用dnspod解析域名

### 功能说明
> 后台

1. 使用java 动态去控制nginx 的跳转和配置
2. 使用jsoup 实现简单扒网页
3. 使用dnspod 实现动态的解析A记录类型域名
4. 使用七牛云实现图床系统
5. 添加指定模板的多品订单系统
6. 地区屏蔽系统管理
7. 动态修改nginx conf 文件并自动reload
8. 自动分析nginx 日志并入数据库，提供数据库统计
9. 查看链接的访问量

>  前台

1. 使用对应多品订单下单
2. 客户登录查看订单
3. 运营人员登录查看简单的下单信息
4. 地区屏蔽系统接口（ip 地址分析，优选淘宝接口 次选新浪接口）
5. 一个运营账号对应多产品查看（3功能的升级版）


> 使用方法

1. 运行admin  **com.yokead.Application.main** main函数需要一个参数，参数为[config/start_config.json](config/start_config.json)文件的真实路径

2. 运行web  **com.yokead.Application.main** main函数需要一个参数，参数为[config/start_config.json](config/start_config.json)文件的真实路径

