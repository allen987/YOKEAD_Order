package com.yokead;

import cn.jiangzeyin.common.EnableCommonBoot;
import com.yokead.common.YokeadApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * Created by jiangzeyin on 2017/5/9.
 *
 * @author jiangzeyin
 */
@SpringBootApplication
@ServletComponentScan
@EnableCommonBoot
public class Application {
    /**
     * 启动执行
     *
     * @param args 参数
     */
    public static void main(String[] args) throws Exception {
        YokeadApplication.createBuilder("admin", Application.class).run(args);
//        //具体日期
//        String logDate = "2018-10-1";
//        Set<String> list = GetIp.getip(DateTime.of(logDate, DatePattern.NORM_DATE_PATTERN), 16);
//        System.out.println(list);
    }


}
