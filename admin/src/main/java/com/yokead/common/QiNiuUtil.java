package com.yokead.common;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qiniu.util.Auth;

import java.io.File;
import java.io.IOException;

/**
 * Created by jiangzeyin on 2018/10/14.
 */
public class QiNiuUtil {

    public static Auth getAuth() {
        JSONObject images = getImagesConfig();
        String accessKey = images.getString("ak");
        String secretKey = images.getString("sk");
        return Auth.create(accessKey, secretKey);
    }

    public static String getImagesBucket() throws IOException {
        JSONObject images = getImagesConfig();
        return images.getString("bucket");
    }

    /**
     * 获取图床空间域名
     *
     * @return
     */
    public static String getDoMain() {
        JSONObject images = getImagesConfig();
        return images.getString("domain");
    }


    private static JSONObject getImagesConfig() {
        File file = Config.QiNiu.getQiNiuConfigFile();
        String json = FileUtil.readString(file, CharsetUtil.CHARSET_UTF_8);
        JSONObject jsonObject = JSON.parseObject(json);
        JSONObject images = jsonObject.getJSONObject("images");
        if (images == null) {
            throw new IllegalArgumentException("请配置图片库信息");
        }
        return images;
    }
}
