package com.yokead.common.base;

import com.yokead.system.init.InitUserName;

/**
 * @author Administrator
 * date 2017/5/16
 */
public abstract class AdminBaseControl extends AbstractBaseControl {
    protected String userName;

    @Override
    public void resetInfo() {
        super.resetInfo();
        this.userName = InitUserName.getUserName();
    }
}
