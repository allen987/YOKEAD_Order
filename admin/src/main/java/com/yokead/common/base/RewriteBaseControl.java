package com.yokead.common.base;

import com.yokead.service.rewrite.ConfService;

import javax.annotation.Resource;

/**
 * Created by jiangzeyin on 2017/5/11.
 */
public abstract class RewriteBaseControl extends AdminBaseControl {
    @Resource
    private ConfService confService;

    protected void reloadNginx(String reus) {
        confService.reload(userName + " 操作了" + reus);
    }
}
