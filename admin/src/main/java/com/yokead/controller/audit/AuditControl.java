package com.yokead.controller.audit;

import cn.jiangzeyin.common.JsonMessage;
import com.yokead.common.Config;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.common.tools.UrlPath;
import com.yokead.controller.copy.CopyIndexControl;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import com.yokead.util.EncryptUtil;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.IOException;

/**
 * Created by jiangzeyin on 2017/7/5.
 */
@Controller
@RequestMapping("audit")
public class AuditControl extends AdminBaseControl {

    @RequestMapping(value = "index.html", produces = MediaType.TEXT_HTML_VALUE)
    public String index(String name) throws IOException {
        setAttribute("data", CopyIndexControl.getInfo(UrlPath.Type.Status, name, Config.Nginx.getNginxConfigPath()));
        return "audit/index";
    }

    /**
     * 切换文案审核状态
     *
     * @param urlPath
     * @return
     */
    @RequestMapping(value = "change_status.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String change_status(String urlPath) {
        String path;
        try {
            path = EncryptUtil.decrypt(urlPath);
        } catch (Exception e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("系统错误", e);
            return JsonMessage.getString(123, "切换失败,参数错误，请联系管理员");
        }
        File file = new File(path);
        if (!file.exists())
            return JsonMessage.getString(256, "切换失败,文件不存在");
        File index = new File(file, "index.html");
        if (!index.exists())
            return JsonMessage.getString(256, "切换失败,文件index不存在");
        File sh = new File(file, "index_sh.html");
        File tf = new File(file, "index_tf.html");
        if (sh.exists()) {
            try {
                FileCopyUtils.copy(index, tf);
                FileCopyUtils.copy(sh, index);
                if (!sh.delete())
                    return JsonMessage.getString(500, "清理资源失败：sh");
            } catch (Exception e) {
                SystemLog.LOG(LogType.CONTROL_ERROR).error("系统错误", e);
                return JsonMessage.getString(500, "切换失败，请联系管理员");
            }
        } else if (tf.exists()) {
            try {
                FileCopyUtils.copy(index, sh);
                FileCopyUtils.copy(tf, index);
                if (!tf.delete())
                    return JsonMessage.getString(500, "清理资源失败:tf");
            } catch (Exception e) {
                SystemLog.LOG(LogType.CONTROL_ERROR).error("系统错误", e);
                return JsonMessage.getString(500, "切换失败，请联系管理员");
            }
        } else {
            return JsonMessage.getString(400, "切换失败，没有对应状态");
        }
        return JsonMessage.getString(200, "切换成功");
    }
}
