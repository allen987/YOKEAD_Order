package com.yokead.controller.copy;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.http.HtmlUtil;
import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.Config;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.common.base.DiggingBaseControl;
import com.yokead.common.tools.UrlPath;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import com.yokead.util.EncryptUtil;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by jiangzeyin on 2017/7/3.
 */
@Controller
@RequestMapping("copy")
public class CopyIndexControl extends AdminBaseControl {

    @RequestMapping(value = "index.html", produces = MediaType.TEXT_HTML_VALUE)
    public String index(String name) throws IOException {
        setAttribute("data", getInfo(UrlPath.Type.None, name, Config.Nginx.getNginxConfigPath()));
        return "copy/index";
    }

    public static JSONObject getInfo(UrlPath.Type type, String name, String path) throws IOException {
        JSONArray jsonArray = UrlPath.start(path, type);
        if (jsonArray == null) {
            return null;
        }
        HashMap<String, JSONArray> jsonObjectHashMap = new HashMap<>();
        for (int i = jsonArray.size() - 1; i >= 0; i--) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String userName = jsonObject.getString("userName");
            if (!StringUtil.isEmpty(name) && !"全部".equals(name)) {
                if (!userName.equals(name)) {
                    continue;
                }
            }

            String url = jsonObject.getString("urls");
            if (StringUtil.isEmpty(url)) {
                url = jsonObject.getString("url");
                int s = url.indexOf("://");
                String tempStr = url.substring(s + 1);
                url = tempStr.substring(0, tempStr.indexOf("/"));
            }
            JSONArray jsonArray1 = jsonObjectHashMap.computeIfAbsent(url, k -> new JSONArray());
            jsonArray1.add(jsonObject);
        }
        return (JSONObject) JSONObject.toJSON(jsonObjectHashMap);
    }


    @RequestMapping(value = "getType.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String save_weixin() {
        File file = new File(Config.Nginx.getNginxConfigPath());
        File[] files = file.listFiles((dir, name) -> new File(dir, name).isDirectory());
        if (files == null) {
            return JsonMessage.getString(100, "没有数据");
        }
        JSONArray jsonArray = new JSONArray();
        for (File item : files) {
            jsonArray.add(item.getName());
        }
        return JsonMessage.getString(200, "", jsonArray);
    }

    /**
     * @param tag
     * @return
     */
    @RequestMapping(value = "edit_{tag}.html", produces = MediaType.TEXT_HTML_VALUE)
    public String edit(@PathVariable String tag) {
        String path;
        try {
            path = EncryptUtil.decrypt(tag);
            File file = new File(path);
            if (!file.exists()) {
                setAttribute("tip", "编辑失败,文件不存在，请联系管理员");
                return "copy/edit";
            }
            //String context = EncodeFileUtil.readToString(file);
            //if (StringUtil.isEmpty(context)) {
            // request.setAttribute("tip", "编辑失败,文件内容为null，请联系管理员");
            // return "copy/edit";
            //}
            setAttribute("tag_", tag);
            //request.setAttribute("context", context);
        } catch (Exception e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("编辑失败", e);
            setAttribute("tip", "编辑失败,参数错误，请联系管理员:" + e.getLocalizedMessage());
            return "copy/edit";
        }
        return "copy/edit";
    }

    @RequestMapping(value = "getContext.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String save_weixin(String tag) {
        String path;
        try {
            path = EncryptUtil.decrypt(tag);
            File file = new File(path);
            if (!file.exists()) {
                return JsonMessage.getString(400, "获取失败,tag 不正确");
            }
            String context = FileUtil.readString(file, CharsetUtil.CHARSET_UTF_8);
            context = HtmlUtil.unescape(StringUtil.compileHtml(context));
            return JsonMessage.getString(200, "", context);
        } catch (Exception e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("获取失败", e);
            return JsonMessage.getString(500, "获取失败," + e.getMessage());
        }
    }

    @RequestMapping(value = "save_context.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String save_weixin(String context, String tag) {
        String path;
        try {
            path = EncryptUtil.decrypt(tag);
            File file = new File(path);
            if (!file.exists()) {
                return JsonMessage.getString(100, "保存失败,文件不存在，请联系管理员");
            }
            if (StringUtil.isEmpty(context)) {
                return JsonMessage.getString(150, "请输入内容");
            }
            context = HtmlUtil.unescape(StringUtil.compileHtml(context));
            DiggingBaseControl.backIndexHtml(file.getParentFile());
            FileUtil.writeString(context, path, CharsetUtil.CHARSET_UTF_8);
            return JsonMessage.getString(200, "保存成功");
        } catch (Exception e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("保存失败", e);
            return JsonMessage.getString(100, "保存失败,请选择正确的文件");
        }
    }
}
