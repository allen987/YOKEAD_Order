package com.yokead.controller.digging.template;

import com.yokead.common.base.AdminBaseControl;
import com.yokead.controller.images.ImagesControl;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by jiangzeyin on 2018/6/28.
 */
@Controller
@RequestMapping("digging/template")
public class ListUploadController extends AdminBaseControl {
    /**
     * 上传接口
     *
     * @return json
     */
    @RequestMapping(value = "upload", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String upload() {
        return ImagesControl.upload(getMultiRequest(), userName);
    }
}
