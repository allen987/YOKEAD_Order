package com.yokead.controller.images;

import cn.hutool.core.io.FileUtil;
import cn.jiangzeyin.Md5Util;
import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.Config;
import com.yokead.common.QiNiuFile;
import com.yokead.common.QiNiuUtil;
import com.yokead.common.base.DiggingBaseControl;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.security.NoSuchAlgorithmException;

/**
 * Created by jiangzeyin on 2017/7/11.
 */
@Controller
@RequestMapping("images")
public class FileControl extends DiggingBaseControl {
    /**
     * @return
     */
    @RequestMapping(value = "file.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String fileIndex() {
        return "images/file";
    }

    /**
     * 接收上传文件
     *
     * @param urls
     * @param quality
     * @return
     */
    @RequestMapping(value = "file", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String file(String urls, String quality) throws NoSuchAlgorithmException {
        String[] urls_ = StringUtil.stringToArray(urls, "\r\n");
        if (urls_ == null || urls_.length <= 0) {
            return JsonMessage.getString(400, "没有需要上传的文件");
        }
        String doMain = QiNiuUtil.getDoMain();
        JSONArray jsonArray = new JSONArray();
        float Quality = StringUtil.parseFloat(quality);
        if (Quality == 0.0F) {
            Quality = 0.7f;
        } else if ((Quality != 100F) && (Quality <= 0.0F || Quality >= 1.0F)) {
            Quality = 0.7f;
        }
        File path = new File(String.format("%s/temp/upload/file/%s", Config.Admin.getTempDir(), userName));
        FileUtil.clean(path);
        for (String item : urls_) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", item);
            jsonArray.add(jsonObject);
            if (StringUtil.isEmpty(item)) {
                jsonObject.put("msg", "不能为空");
                continue;
            }
            if (!item.startsWith("http")) {
                jsonObject.put("msg", "不能为空，没有http");
                continue;
            }
            String mName = Md5Util.getString(item);
            String fileName = String.format("%s/%s", path.getPath(), mName);
            try {
                boolean flag = httpDownload(item, fileName, true);
                if (!flag) {
                    jsonObject.put("msg", "下载失败");
                    continue;
                }
                File file = new File(fileName);
                if (!file.exists()) {
                    jsonObject.put("msg", "下载失败:-1");
                    continue;
                }
            } catch (Exception e) {
                SystemLog.LOG(LogType.CONTROL_ERROR).error("下载文件异常", e);
                jsonObject.put("msg", e.getMessage());
                continue;
            }
            String md5;
            try {
                md5 = QiNiuFile.doUploadFile(fileName, Quality);
            } catch (Exception e) {
                SystemLog.LOG(LogType.CONTROL_ERROR).error("上传文件异常", e);
                jsonObject.put("msg", e.getMessage());
                continue;
            }
            SystemLog.LOG().info(userName + " 上传：" + md5);
            jsonObject.put("url", StringUtil.clearPath(String.format("%s/%s", doMain, md5)));
        }
        return JsonMessage.getString(200, "处理结果", jsonArray);
    }
}
