package com.yokead.controller.order;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.jiangzeyin.DateUtil;
import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.druid.util.JdbcUtils;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.Config;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.database.DatabaseContextHolder;
import com.yokead.service.order.KhService;
import com.yokead.service.order.OrderService;
import com.yokead.service.order.ProductService;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import com.yokead.util.JsonUtil;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.regex.Pattern;

/**
 * Created by jiangzeyin on 2017/5/18.
 */
@Controller
@RequestMapping("order")
public class EditProductControl extends AdminBaseControl {

    private static final String KH_DEL = "_del";

    @Resource
    private KhService khService;
    @Resource
    private ProductService productService;
    @Resource
    private OrderService orderService;

    /**
     * 创建产品页面
     *
     * @param name
     * @param type
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "create.html", produces = MediaType.TEXT_HTML_VALUE)
    public String create(String name, String type) throws Exception {
        JSONObject jsonObject = orderService.getOrderInfo(type);
        if (jsonObject == null) {
            return "redirect:index.html";
        }
        String path = jsonObject.getString("vmPath");
        return "order/" + path;
    }

    @RequestMapping(value = "chang_product_sort", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String chang(String name, String productName, String to) throws IOException {
        name = convertFilePath(name);
        JSONArray jsonArray = productService.getUserProductJson(name);
        if (jsonArray == null) {
            return JsonMessage.getString(300, "数据异常");
        }
        ProductService.sort(jsonArray);
        boolean flag = false;
        int count = jsonArray.size();
        for (int i = 0; i < count; i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String name_ = jsonObject.getString("name");
            if (name_.equals(productName)) {
                int sort = 0;
                if ("up".endsWith(to)) {
                    if (i == 0) {
                        return JsonMessage.getString(405, "已经在最上面啦");
                    }
                    sort = jsonArray.getJSONObject(i - 1).getIntValue("sort") - 1;
                } else if ("down".equals(to)) {
                    if (count - 1 == i) {
                        return JsonMessage.getString(405, "已经在最底部啦");
                    }
                    sort = jsonArray.getJSONObject(i + 1).getIntValue("sort") + 1;
                }
                jsonObject.put("sort", sort);
                flag = true;
            }
        }
        if (!flag) {
            return JsonMessage.getString(300, "改变失败");
        }
        File file = productService.getUserProductFile(name);
        JsonUtil.saveJson(file.getPath(), jsonArray);
        return JsonMessage.getString(200, "ok");
    }

    /**
     * 删除产品
     *
     * @param name
     * @param productName
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "del_product.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String del_product(String name, String productName) throws Exception {
        name = convertFilePath(name);
        JSONArray jsonArray = productService.getUserProductJson(name);
        if (jsonArray == null) {
            return JsonMessage.getString(300, "数据异常");
        }
        boolean flag = false;
        for (int i = jsonArray.size() - 1; i >= 0; i--) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String name_ = jsonObject.getString("name");
            if (name_.equals(productName)) {
                jsonObject.put("name", name_ + KH_DEL);
                jsonObject.put("delTime", DateUtil.getCurrentFormatTime("yyyyMMdd"));
                flag = true;
            }
        }
        if (!flag) {
            return JsonMessage.getString(300, "删除失败，没有对应数据");
        }
        File file = productService.getUserProductFile(name);
        JsonUtil.saveJson(file.getPath(), jsonArray);
        File old = productService.getUserProductPath(name, productName);
        File newFile = new File(old.getParent(), old.getName() + KH_DEL);
        flag = old.renameTo(newFile);
        if (flag) {
            return JsonMessage.getString(200, "删除成功");
        }
        return JsonMessage.getString(100, "删除失败");
    }

    @RequestMapping(value = "edit_save.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String create_save(String khName, String name, String mark) throws IOException {
        khName = convertFilePath(khName);
        name = convertFilePath(name);
        if (StringUtil.isChinese(name)) {
            return JsonMessage.getString(400, "产品标识不能包含中文");
        }
        boolean flag = khService.checkKh(userName, khName);
        if (!flag) {
            return JsonMessage.getString(400, "请正确使用系统");
        }
        JSONArray jsonArray = productService.getUserProductJson(khName);
        flag = false;
        for (int i = jsonArray.size() - 1; i >= 0; i--) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String name_ = jsonObject.getString("name");
            if (name_.equals(name)) {
                flag = true;
                jsonObject.put("mark", mark);
                break;
            }
        }
        if (!flag) {
            return JsonMessage.getString(400, "修改失败，不存在改产品");
        }
        // 验证目录
        // new File(productService.getCustomerFilePath(), khName + "/" + name);
        File temp = productService.getUserProductPath(khName, name);
        if (!temp.exists()) {
            return JsonMessage.getString(400, "修改失败，不存在改产品,目录");
        }
        // 修改mark
        // 检查产品是否正确
//        JSONArray jsonProducts = new JSONArray();
//        JSONArray jsonfahusS = new JSONArray();
//        Boolean cashOnDelivery = true;
        Object objectTip = getIn();
        if (objectTip instanceof String) {
            return objectTip.toString();
        }
        JSONObject jsonObject = (JSONObject) objectTip;
        try {
            productService.saveUserProduct(khName, jsonArray);
        } catch (IOException e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("保存产品异常", e);
            return JsonMessage.getString(400, "保存产品异常");
        }
//        JSONObject jsonObject = productService.getProductInfoJsonObj(khName, name);
//        jsonObject.put("title", title);
//        jsonObject.put("product", jsonProducts);
//        jsonObject.put("fahu", jsonfahusS);
        //  new File(temp, "product.json");
        File pJson = productService.getProductInfoJsonFile(khName, name);
        JsonUtil.saveJson(pJson.getPath(), jsonObject);
        return JsonMessage.getString(200, "修改成功");
    }

    /**
     * 确定创建产品
     *
     * @param name
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "create_save.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String create_save(String khName, String orderType, String name, String mark) throws IOException {
        khName = convertFilePath(khName);
        name = convertFilePath(name);
        if (StringUtil.isChinese(name)) {
            return JsonMessage.getString(400, "产品标识不能包含中文");
        }
        if (!Pattern.matches("^[0-9a-zA-Z_]+$", name)) {
            return JsonMessage.getString(400, "产品标识只能是数字,字母或者 '_'");
        }
        boolean flag = khService.checkKh(userName, khName);
        if (!flag) {
            return JsonMessage.getString(400, "请正确使用系统");
        }

        JSONArray jsonArray = productService.getUserProductJson(khName);
        if (jsonArray != null) {
            for (int i = jsonArray.size() - 1; i >= 0; i--) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String name_ = jsonObject.getString("name");
                if (name_.equals(name)) {
                    return JsonMessage.getString(400, "已经存在相同的产品标识");
                }
                if (name_.equals(name + KH_DEL)) {
                    return JsonMessage.getString(400, "此产品标识已经被使用过，现已被删除");
                }
            }
        }
        JSONObject orderInfo = orderService.getOrderInfo(orderType);
        if (orderInfo == null) {
            return JsonMessage.getString(400, "使用错误，请检查");
        }
        // 验证目录
        //new File(productService.getCustomerFilePath(), khName + "/" + name);
        File productDir = productService.getUserProductPath(khName, name);
        if (productDir.exists()) {
            return JsonMessage.getString(400, "该产品标识已被占用");
        }
        // new File(productService.getCustomerFilePath(), khName);
        File checkZm = productService.getUserPath(khName);
        String finalName = name;
        String[] xiangTongFile = checkZm.list((dir, fileName) -> fileName.equalsIgnoreCase(finalName) ||
                fileName.equalsIgnoreCase(finalName + KH_DEL));
        if (xiangTongFile != null && xiangTongFile.length > 0) {
            return JsonMessage.getString(402, "该产品名称文件已被占用");
        }
        // 检查产品是否正确
//        JSONArray jsonProducts = new JSONArray();
//        JSONArray jsonfahusS = new JSONArray();
//        Boolean cashOnDelivery = true;
        Object objectTip = getIn();
        if (objectTip instanceof String) {
            return objectTip.toString();
        }
        JSONObject object = (JSONObject) objectTip;
//        String r = getIn(products, fahus, jsonProducts, jsonfahusS, cashOnDelivery);
//        if (r != null) {
//            return r;
//        }
        // 创建对象
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", name);
        jsonObject.put("orderType", orderType);
        jsonObject.put("createTime", DateUtil.getCurrentFormatTime("yyyyMMdd"));
        String tableName = String.format("%s_%s_%s", userName, khName, name).replace('-', '_');
        jsonObject.put("tableName", tableName);
        jsonObject.put("mark", mark);
        if (jsonArray == null) {
            jsonArray = new JSONArray();
        }
        jsonArray.add(jsonObject);
        try {
            productService.saveUserProduct(khName, jsonArray);
        } catch (IOException e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("保存异常", e);
            return JsonMessage.getString(400, "保存产品异常");
        }
        //File file = productService.getUserProductFile(khName);
        if (!productDir.exists() && !productDir.mkdirs()) {
            return JsonMessage.getString(405, "文件信息初始化失败，请联系管理员");
        }
        File pJson = new File(productDir, "product.json");

        JsonUtil.saveJson(pJson.getPath(), object);
        File sqlFile = new File(Config.Order.getOrderTypePath(), String.format("%s.sql", orderType));
        if (!sqlFile.exists() && !sqlFile.isFile()) {
            return JsonMessage.getString(505, "创建失败，请联系管理员:sql n exists or file");
        }
        String sql = FileUtil.readString(sqlFile, CharsetUtil.CHARSET_UTF_8);
        sql = String.format(sql, tableName);
        DataSource dataSource = DatabaseContextHolder.getDataSource();
        try {
            JdbcUtils.execute(dataSource, sql);
        } catch (SQLException e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("创建失败sql 异常", e);
            return JsonMessage.getString(500, "创建失败，请联系管理员");
        }
        return JsonMessage.getString(200, "创建成功");
    }

    private Object getIn() {
        String title = getParameter("title");
        if (StringUtil.isEmpty(title, 2, 50)) {
            return JsonMessage.getString(400, "请输入正确的网页标题");
        }
        String cashOnDeliveryStr = getParameter("cashOnDelivery", "true");
        Boolean cashOnDelivery = Convert.toBool(cashOnDeliveryStr, true);
        String products = getParameter("products");
        String fahus = getParameter("fahus");
        JSONArray jsonProducts = new JSONArray();
        JSONArray jsonfahusS = new JSONArray();

        // 检查产品是否正确
        String[] products_ = StringUtil.stringToArray(products, "\r\n");
        if (products_ == null || products_.length < 1) {
            return JsonMessage.getString(400, "请输入产品信息");
        }
        boolean pChecked = false;
        for (String item : products_) {
            String[] item_ = StringUtil.stringToArray(item, ",");
            if (item_ == null || item_.length < 2) {
                return JsonMessage.getString(400, "\"" + item + "\",格式不正确");
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", item_[0]);
            jsonObject.put("price", StringUtil.parseInt(item_[1]));
            if (item_.length == 3 && !pChecked) {
                jsonObject.put("checked", StringUtil.parseInt(item_[2]) == 1);
                pChecked = true;
            }
            jsonProducts.add(jsonObject);
        }
        // 检查发货单
        String[] fahus_ = StringUtil.stringToArray(fahus, "\r\n");
        if (fahus_ != null && fahus_.length > 0) {
            // 可以没有发货信息
            for (String item : fahus_) {
                String[] item_ = StringUtil.stringToArray(item, ",");
                if (item_ == null || item_.length != 4) {
                    return JsonMessage.getString(400, "\"" + item + "\",格式不正确");
                }
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("time", item_[0]);
                jsonObject.put("name", item_[1]);
                jsonObject.put("phone", StringUtil.convertNULL(item_[2]));
                jsonObject.put("mark", item_[3]);
                jsonfahusS.add(jsonObject);
            }
        }
        // 
        JSONObject object = new JSONObject();
        object.put("title", title);
        object.put("product", jsonProducts);
        object.put("fahu", jsonfahusS);
        object.put("cashOnDelivery", cashOnDelivery);
        return object;
    }

    @RequestMapping(value = "edit_product.html", produces = MediaType.TEXT_HTML_VALUE)
    public String edit(String name, String productName) throws Exception {
        name = convertFilePath(name);
        productName = convertFilePath(productName);
        JSONObject jsonObject = productService.getUserProductJsonObj(name, productName);
        if (jsonObject == null) {
            return "redirect:index.html";
        }
        String type = jsonObject.getString("orderType");
        JSONObject orderInfo = orderService.getOrderInfo(type);
        if (orderInfo == null) {
            return "redirect:index.html";
        }
        String path = orderInfo.getString("vmPath");
        JSONObject item = productService.getProductInfoJsonObj(name, productName);
        JSONArray product = item.getJSONArray("product");
        StringBuffer products = new StringBuffer();
        for (int i = 0; i < product.size(); i++) {
            JSONObject temp = product.getJSONObject(i);
            products.append(temp.getString("name")).append(",").append(temp.getString("price"));
            if (temp.getBooleanValue("checked")) {
                products.append(",").append("1");
            }
            products.append("\r\n");
        }
        item.put("product", products);
        JSONArray fahu = item.getJSONArray("fahu");
        StringBuffer fahus = new StringBuffer();
        for (int i = fahu.size() - 1; i >= 0; i--) {
            JSONObject temp = fahu.getJSONObject(i);
            fahus.append(temp.getString("time")).append(",")
                    .append(temp.getString("name")).append(",")
                    .append(temp.getString("phone")).append(",")
                    .append(temp.getString("mark"));
            fahus.append("\r\n");
        }
        item.put("fahu", fahus);
        setAttribute("item", item);
        setAttribute("itemConf", jsonObject);
        setAttribute("productName", productName);
        return "order/" + path;
    }
}
