package com.yokead.controller.order;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.service.order.ProductService;
import com.yokead.util.JsonUtil;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created by jiangzeyin on 2017/5/15.
 */
@Controller
@RequestMapping("order")
public class OrderIndexControl extends AdminBaseControl {
    @Resource
    private ProductService productService;

    /**
     * 获取自己的客户
     *
     * @return 页面
     * @throws IOException io
     */
    @RequestMapping(value = "index.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String index() throws IOException {
        String filePath = productService.getCustomerInfoFile().getPath();
        JSONArray jsonArray = (JSONArray) JsonUtil.readJson(filePath);
        JSONArray newJson = new JSONArray();
        for (int i = jsonArray.size() - 1; i >= 0; i--) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String parent = jsonObject.getString("parent");
            if (userName.equals(parent)) {
                newJson.add(jsonObject);
            }
        }
        ProductService.sort(newJson);
        setAttribute("data", newJson);
        return "order/index";
    }
}
