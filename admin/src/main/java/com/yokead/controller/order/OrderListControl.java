package com.yokead.controller.order;

import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.service.order.OrderService;
import com.yokead.service.order.ProductService;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created by jiangzeyin on 2017/5/18.
 */
@Controller
@RequestMapping("order")
public class OrderListControl extends AdminBaseControl {
    @Resource
    private ProductService productService;

    @Resource
    private OrderService orderService;

    /**
     * @param name
     * @param product
     * @return
     */
    @RequestMapping(value = "list_data.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String index(String name, String product) {
        name = convertFilePath(name);
        product = convertFilePath(product);
        JSONObject jsonOrder = orderService.getProductColumn(name, product);
        if (jsonOrder == null)
            return "order/list_data";
        setAttribute("columns", jsonOrder.getJSONArray("columns"));
        setAttribute("name", name);
        setAttribute("product", product);
        //String sql = jsonOrder.getString("sqlColumn");
        return "order/list_data";
    }

    @RequestMapping(value = "list_data_all.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String list_data_all(String name, String product, String pageNo, String pageSize, String type) {
        name = convertFilePath(name);
        product = convertFilePath(product);
        int pageNoInt = StringUtil.parseInt(pageNo, 1);
        int pageSizeInt = StringUtil.parseInt(pageSize, 10);
        int dataType = StringUtil.parseInt(type, 0);
        return orderService.doSelect(name, product, pageNoInt, pageSizeInt, null, dataType, null, null, null);
    }

    /**
     * 作废
     *
     * @param product
     * @param data
     * @return
     */
    @RequestMapping(value = "zf.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String listData(String name, String product, String data, String type) {
        product = convertFilePath(product);
        name = convertFilePath(name);
        int dataType = StringUtil.parseInt(type, 0);
        if (dataType == 0)
            dataType = 1;
        else if (dataType == 1)
            dataType = 0;
        String tableName;
        try {
            tableName = productService.getProductTableName(name, product);
        } catch (IOException e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("作废失败", e);
            return JsonMessage.getString(500, "作废失败");
        }
        return orderService.zf(tableName, data, null, dataType);
    }
}
