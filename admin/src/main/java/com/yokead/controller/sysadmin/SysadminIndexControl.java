package com.yokead.controller.sysadmin;

import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.base.AdminBaseControl;
import com.yokead.service.rewrite.RewriteIndexService;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

/**
 * Created by jiangzeyin on 2017/5/9.
 */
@Controller
@RequestMapping("sysadmin")
public class SysadminIndexControl extends AdminBaseControl {

    @Resource
    RewriteIndexService rewriteIndexService;

    @RequestMapping(value = "index.html", produces = MediaType.TEXT_HTML_VALUE)
    public String index() {
        String[] names = rewriteIndexService.listNames();
        setAttribute("names", names);
        return "sysadmin/index";
    }

    @RequestMapping(value = "getConf.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String getConf(String name) {
        if (StringUtil.isEmpty(name))
            return JsonMessage.getString(400, "请正确使用");
//        if (!name.endsWith(".conf"))
//            return JsonMessage.getString(400, "请正确使用,参数");
        File c = new File(rewriteIndexService.getConfFile(), name);
        if (!c.exists())
            return JsonMessage.getString(201, "没有数据");
        return JsonMessage.getString(200, "成功", c.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".conf");
            }
        }));
    }


    @RequestMapping(value = "getUrl.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String getUrl(String parent, String name) {
        if (StringUtil.isEmpty(name))
            return JsonMessage.getString(400, "请正确使用");
        if (!name.endsWith(".conf"))
            return JsonMessage.getString(400, "请正确使用,参数");
        File c = new File(rewriteIndexService.getConfFile(), parent + "/" + name);
        if (!c.exists())
            return JsonMessage.getString(201, "没有数据");
        try {
            JSONObject jsonObject = rewriteIndexService.getConfRoot(c.getPath());
            if (jsonObject == null)
                return JsonMessage.getString(202, "没有数据");
            String rootPath = jsonObject.getString("rootPath");
            JSONObject data = new JSONObject();
            if (!StringUtil.isEmpty(rootPath)) {
                File paths = new File(rootPath);// NgxConfig conf = NgxConfig.read(c.getPath());
                if (paths.exists())
                    data.put("path", paths.list());
            }
            data.put("url", jsonObject.getJSONArray("server_name"));
            //return JsonMessage.getString(203, "没有数据");
            return JsonMessage.getString(200, "成功", data);
        } catch (IOException e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("异常", e);
        }
        return JsonMessage.getString(500, "获取失败");
    }


}
