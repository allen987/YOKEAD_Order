package com.yokead.service.order;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.service.BaseService;
import com.yokead.util.JsonUtil;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

/**
 * Created by jiangzeyin on 2017/5/17.
 */
@Service
public class KhService extends BaseService {

    public boolean checkKh(String parent, String name) throws IOException {
        File file = getCustomerInfoFile();
        JSONArray jsonArray = (JSONArray) JsonUtil.readJson(file.getPath());
        for (int i = jsonArray.size() - 1; i >= 0; i--) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String parent_ = jsonObject.getString("parent");
            String name_ = jsonObject.getString("name");
            if (parent_.equals(parent) && name_.equals(name)) {
                return true;
            }
        }
        return false;
    }
}
