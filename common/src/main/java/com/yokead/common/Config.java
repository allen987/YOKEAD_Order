package com.yokead.common;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.jiangzeyin.StringUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Created by jiangzeyin on 2018/5/9.
 */
public class Config {
    private static JSONObject ConfigJson;
    private static String configJson;
    private static long lastModified;
    private static String module;

    public static void config(String configJson, String module) throws IOException {
        Config.configJson = configJson;
        Config.module = module;
        File file = new File(configJson);
        if (!file.exists()) {
            throw new IllegalArgumentException(configJson + " 配置文件不存在");
        }
        if (!file.isFile()) {
            throw new IllegalArgumentException(configJson + " 不是一个正确的配置文件");
        }
        long lastModified = file.lastModified();
        if (lastModified == Config.lastModified) {
            return;
        }
        if (Config.lastModified != 0) {
            System.out.println("重新加载配置文件");
        }
        Config.lastModified = lastModified;
        String json = FileUtil.readString(file, CharsetUtil.CHARSET_UTF_8);
        Config.ConfigJson = JSONObject.parseObject(json);
        Admin.admin = ConfigJson.getJSONObject("admin");
        if (Admin.admin == null) {
            throw new IllegalArgumentException("请配置admin 属性");
        }
        Order.order = ConfigJson.getJSONObject("order");
        if (Order.order == null) {
            throw new IllegalArgumentException("请配置 order 属性");
        }
        Shield.shield = ConfigJson.getJSONObject("shield");
        DnsPodConfig.dnspod = ConfigJson.getJSONObject("dnspod");
        Nginx.nginx = ConfigJson.getJSONObject("nginx");
        QiNiu.qiniu = ConfigJson.getJSONObject("qiniu");
        Web.web = ConfigJson.getJSONObject("web");
        DbConfig.config = ConfigJson.getJSONObject("dbConfig");
        if (DbConfig.config == null) {
            throw new IllegalArgumentException("请配置dbConfig");
        }
        boolean auto_load_config = Admin.admin.getBooleanValue("auto_load_config");
        if (auto_load_config) {
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    try {
                        config(Config.configJson, Config.module);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }, TimeUnit.MINUTES.toMillis(5));
        }
    }

    public static int getPort() {
        int port = 0;
        if ("admin".equals(module)) {
            port = Admin.admin.getIntValue("port");
        } else if ("web".equals(module)) {
            port = Web.web.getIntValue("port");
        }
        if (port <= 0) {
            port = 1122;
        }
        return port;
    }

    public static class Admin {
        private static JSONObject admin;


        /**
         * 后台用户信息配置文件
         *
         * @return path
         */
        public static String getUserConfigFile() {
            String file = admin.getString("user_info");
            if (StringUtil.isEmpty(file)) {
                throw new IllegalArgumentException("请配置 user_info 属性");
            }
            File file1 = new File(file);
            if (!file1.exists() && !file1.isFile()) {
                throw new IllegalArgumentException(file + " 不存在或者不是一个文件");
            }
            return file;
        }

        /**
         * 临时目录
         *
         * @return path
         */
        public static String getTempDir() {
            String file = admin.getString("temp_dir");
            if (StringUtil.isEmpty(file)) {
                throw new IllegalArgumentException("请配置 temp_dir 属性");
            }
            File file1 = new File(file);
            if (!file1.exists() && !file1.isDirectory()) {
                throw new IllegalArgumentException(file + " 不存在或者不是一个目录");
            }
            return file;
        }

        public static JSONObject getDefaultUser() {
            JSONObject jsonObject = admin.getJSONObject("default_user");
            if (jsonObject == null) {
                throw new IllegalArgumentException("请配置默认用户信息");
            }
            return jsonObject;
        }

        public static File getPageTemplatePath() {
            String file = admin.getString("page_template");
            if (StringUtil.isEmpty(file)) {
                throw new IllegalArgumentException("请配置 page_template 属性");
            }
            File file1 = new File(file);
            if (!file1.exists() && !file1.isDirectory()) {
                throw new IllegalArgumentException(file + " 不存在或者不是一个目录");
            }
            return file1;
        }

        /**
         * 默认ip
         *
         * @return ip
         */
        public static String getDefaultIp() {
            return admin.getString("default_ip");
        }
    }


    public static class QiNiu {
        private static JSONObject qiniu;

        /**
         * 七牛云配置文件
         *
         * @return file
         */
        public static File getQiNiuConfigFile() {
            if (qiniu == null) {
                throw new IllegalArgumentException("请配置 qiniu 属性");
            }
            String file = qiniu.getString("config_path");
            if (StringUtil.isEmpty(file)) {
                throw new IllegalArgumentException("请配置 qiniu_config 属性");
            }
            File file1 = new File(file);
            if (!file1.exists() && !file1.isFile()) {
                throw new IllegalArgumentException(file + " 不存在或者不是一个文件");
            }
            return file1;
        }
    }

    public static class Nginx {

        private static JSONObject nginx;

        /**
         * nginx 配置目录
         *
         * @return path
         */
        public static String getNginxConfigPath() {
            String path = nginx.getString("config_path");
            if (StringUtil.isEmpty(path)) {
                throw new IllegalArgumentException("请配置 config_path 属性");
            }
            File file = new File(path);
            if (!file.exists() && !file.isDirectory()) {
                throw new IllegalArgumentException(path + " 不存在或者不是一个目录");
            }
            return path;
        }

        /**
         * nginx 的模板
         *
         * @return file
         */
        public static File getNginxDefaultConfigTemplateFile() {
            String file = nginx.getString("default_config_template");
            if (StringUtil.isEmpty(file)) {
                throw new IllegalArgumentException("请配置 default_config_template 属性");
            }
            File file1 = new File(file);
            if (!file1.exists() && !file1.isFile()) {
                throw new IllegalArgumentException(file + " 不存在或者不是一个文件");
            }
            return file1;
        }

        public static String getStaticPath() {
            String path = nginx.getString("static_path");
            if (StringUtil.isEmpty(path)) {
                throw new IllegalArgumentException("请配置 static_path 属性");
            }
            File file = new File(path);
            if (!file.exists() && !file.isDirectory()) {
                throw new IllegalArgumentException(path + " 不存在或者不是一个目录");
            }
            return path;
        }

        /**
         * @return
         */
        public static String getLogPath() {
            String path = nginx.getString("log_path");
            if (StringUtil.isEmpty(path)) {
                throw new IllegalArgumentException("请配置 static_path 属性");
            }
            File file = new File(path);
            if (!file.exists() && !file.isDirectory()) {
                throw new IllegalArgumentException(path + " 不存在或者不是一个目录");
            }
            return path;
        }

        public static JSONArray getExcludeResolvePath() {
            return nginx.getJSONArray("exclude_resolve_path");
        }
    }

    public static class Order {
        private static JSONObject order;

        /**
         * 订单信息配置路径
         *
         * @return file
         */
        public static File getOrderConfigPath() {
            String path = order.getString("config_path");
            if (StringUtil.isEmpty(path)) {
                throw new IllegalArgumentException("请配置 order_config_path 属性");
            }
            File file = new File(path);
            if (!file.exists() && !file.isDirectory()) {
                throw new IllegalArgumentException(path + " 不存在或者不是一个目录");
            }
            return file;
        }

        /**
         * 订单样式配置路径
         *
         * @return file
         */
        public static File getOrderTypePath() {
            String path = order.getString("type_path");
            if (StringUtil.isEmpty(path)) {
                throw new IllegalArgumentException("请配置 order_type_path 属性");
            }
            File file = new File(path);
            if (!file.exists() && !file.isDirectory()) {
                throw new IllegalArgumentException(path + " 不存在或者不是一个目录");
            }
            return file;
        }

        public static String getOperUrlPrefix() {
            String path = order.getString("oper_url_prefix");
            if (StringUtil.isEmpty(path)) {
                throw new IllegalArgumentException("请配置 oper_url_prefix 属性");
            }
            return path;
        }
    }

    public static class Shield {
        private static JSONObject shield;

        /**
         * 获取屏蔽信息目录
         *
         * @return path
         */
        public static String getShieldPath() {
            if (shield == null) {
                throw new IllegalArgumentException("请配置 shield 属性");
            }
            String path = shield.getString("config_path");
            if (StringUtil.isEmpty(path)) {
                throw new IllegalArgumentException("请配置 config_path 属性");
            }
            File file = new File(path);
            if (!file.exists() && !file.isDirectory()) {
                throw new IllegalArgumentException(path + " 不存在或者不是一个目录");
            }
            return path;
        }

        /**
         * 屏蔽清空缓存接口
         *
         * @return path
         */
        public static String getClearCache() {
            if (shield == null) {
                throw new IllegalArgumentException("请配置 shield 属性");
            }
            String path = shield.getString("clear_cache");
            if (StringUtil.isEmpty(path)) {
                throw new IllegalArgumentException("请配置 clear_cache 属性");
            }
            return path;
        }

        /**
         * nginx 代理接口
         *
         * @return path
         */
        public static String getNginxProxyPass() {
            if (shield == null) {
                throw new IllegalArgumentException("请配置 shield 属性");
            }
            String path = shield.getString("nginx_proxy_pass");
            if (StringUtil.isEmpty(path)) {
                throw new IllegalArgumentException("请配置 nginx_proxy_pass 属性");
            }
            return path;
        }
    }


    public static class DnsPodConfig {

        private static JSONObject dnspod;

        /**
         * dns pod
         *
         * @return file
         */
        public static File getDnsPodConfigFile() {
            if (dnspod == null) {
                throw new IllegalArgumentException("请配置dnspod");
            }
            String file = dnspod.getString("config_path");
            if (StringUtil.isEmpty(file)) {
                throw new IllegalArgumentException("请配置 config_path 属性");
            }
            File file1 = new File(file);
            if (!file1.exists() && !file1.isFile()) {
                throw new IllegalArgumentException(file + " 不存在或者不是一个文件");
            }
            return file1;
        }

        public static String getDnsPodDomainListFile() {
            if (dnspod == null) {
                throw new IllegalArgumentException("请配置dnspod");
            }
            String file = dnspod.getString("domain_list");
            if (StringUtil.isEmpty(file)) {
                throw new IllegalArgumentException("请配置 domain_list 属性");
            }
            File file1 = new File(file);
            if (!file1.exists() && !file1.isFile()) {
                throw new IllegalArgumentException(file + " 不存在或者不是一个文件");
            }
            return file;
        }

        public static File getOptLogPath() {
            if (dnspod == null) {
                throw new IllegalArgumentException("请配置dnspod");
            }
            String file = dnspod.getString("log_path");
            if (StringUtil.isEmpty(file)) {
                throw new IllegalArgumentException("请配置 log_path 属性");
            }
            File file1 = new File(file);
            if (!file1.exists() && !file1.isFile()) {
                throw new IllegalArgumentException(file + " 不存在或者不是一个文件");
            }
            return file1;
        }
    }

    /**
     * web
     */
    public static class Web {
        private static JSONObject web;

        public static String getCopyApiUrl() {
            if (web == null) {
                return null;
            }
            return web.getString("copy_api_url");
        }
    }

    public static class DbConfig {
        private static JSONObject config;

        public static String getDbConfigPath() {
            String file = config.getString("config");
            if (StringUtil.isEmpty(file)) {
                throw new IllegalArgumentException("请配置 config 属性");
            }
            return file;
        }
    }
}
