package com.yokead.service.order;

import cn.jiangzeyin.common.spring.SpringUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.Config;
import com.yokead.service.BaseService;
import com.yokead.util.JsonUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by jiangzeyin on 2017/5/17.
 */
@Service
public class ProductService extends BaseService {

    @Resource
    private OrderService orderService;

    /**
     * 获取表名
     *
     * @param userName
     * @param productName
     * @return
     * @throws IOException
     */
    public String getProductTableName(String userName, String productName) throws IOException {
        JSONObject jsonObject = getUserProductJsonObj(userName, productName);
        if (jsonObject == null)
            return null;
        return jsonObject.getString("tableName");
    }

    /**
     * 获取用户产品配置目录
     *
     * @param userName
     * @param productName
     * @return
     */
    public File getUserProductPath(String userName, String productName) {
        File customer = getUserPath(userName);
        return new File(customer, productName);
    }

    public File getUserPath(String userName) {
        return new File(Config.Order.getOrderConfigPath(), userName);
    }

    /**
     * 获取产品对象信息
     *
     * @param userName
     * @param productName
     * @return
     */
    public File getProductInfoJsonFile(String userName, String productName) {
        File file = getUserProductPath(userName, productName);
        file = new File(file, "product.json");
        if (!file.exists())
            return null;
        return file;
    }

    /**
     * 获取用户产品限制重复
     *
     * @param userName
     * @param productName
     * @return
     */
    public File getProductLimitDuplicateJsonFile(String userName, String productName) {
        File file = getUserProductPath(userName, productName);
        file = new File(file, "limit_duplicate.json");
//        if (!file.exists())
//            return null;
        return file;
    }

    public File getUserProductFile(String userName) {
        File file = getUserPath(userName);
        return new File(file, "product.conf");
    }


    /**
     * 获取客户的产品信息  包括以删除
     *
     * @param name
     * @return
     * @throws IOException
     */
    public JSONArray getUserProductJson(String name) throws IOException {
        File user = getUserProductFile(name);// new File(getCustomerFile(), name + "/product.conf");
        if (!user.exists())
            return null;
        return (JSONArray) JsonUtil.readJson(user.getPath());
    }

    public JSONObject getUserProductJsonObj(String name, String productName) throws IOException {
        JSONArray jsonArray = getUserProductJson(name);
        if (jsonArray == null)
            return null;
        return getJSONObjectName(productName, jsonArray);
    }


    public JSONObject getProductInfoJsonObj(String userName, String productName) throws IOException {
        File file = getProductInfoJsonFile(userName, productName);
        if (file == null)
            return null;
        return (JSONObject) JsonUtil.readJson(file.getPath());
    }


    public void saveUserProduct(String userName, JSONArray jsonArray) throws IOException {
        File product = getUserProductFile(userName);
        JsonUtil.saveJson(product.getPath(), jsonArray);
    }

    /**
     * 获取所有产品  可以使用的
     *
     * @param userName
     * @return
     * @throws IOException
     */
    public JSONArray getUserAll(String userName) throws IOException {
        JSONArray jsonArray = getUserProductJson(userName);
        if (jsonArray == null)
            return null;
        JSONArray jsonArray1 = orderService.getOrderType();
        Iterator<Object> iterable = jsonArray.iterator();
        while (iterable.hasNext()) {
            JSONObject jsonObject = (JSONObject) iterable.next();// jsonArray.getJSONObject(i);
            String name = jsonObject.getString("name");
            if (name.endsWith("_del")) {
                iterable.remove();
                continue;
            }
            String orderType = jsonObject.getString("orderType");
            JSONObject typeObj = getJSONObjectName(orderType, jsonArray1);
            jsonObject.put("orderInfo", typeObj);
        }
        return sort(jsonArray);
    }

    /**
     * 排序
     *
     * @param jsonArray
     * @return
     */
    public static JSONArray sort(JSONArray jsonArray) {
        String time = "sort";
        jsonArray.sort((o1, o2) -> {
            JSONObject jsonObject1 = (JSONObject) o1;
            JSONObject jsonObject2 = (JSONObject) o2;
            int s1 = jsonObject1.getIntValue(time);
            int s2 = jsonObject2.getIntValue(time);
            return s1 - s2;
        });
        return jsonArray;
    }

    /**
     * @param typeName
     * @param jsonArray
     * @return
     */
    public JSONObject getJSONObjectName(String typeName, JSONArray jsonArray) {
        for (int i = jsonArray.size() - 1; i >= 0; i--) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String name = jsonObject.getString("name");
            if (name.equals(typeName))
                return jsonObject;
        }
        return null;
    }


    public static JSONArray getProductLimitDuplicateConfig(String userName, String productName) throws IOException {
        ProductService productService = SpringUtil.getBean(ProductService.class);
        File file = productService.getProductLimitDuplicateJsonFile(userName, productName);
        if (file == null)
            return null;
        if (!file.exists() || file.isDirectory())
            return null;
        return (JSONArray) JsonUtil.readJson(file.getPath());
    }

}
