package com.yokead.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 系统操作工具类
 *
 * @author jiangzeyin
 * @date 2016-9-1
 */
public final class CmdUtil {

    /**
     * 运行命令
     *
     * @param cmdline
     * @return
     * @throws IOException
     * @author jiangzeyin
     * @date 2016-9-1
     */
    public static String cmdExec(String cmdline) throws IOException {
        String line;
        StringBuffer info = new StringBuffer();
        Process p = Runtime.getRuntime().exec(cmdline);
        BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while ((line = input.readLine()) != null) {
            info.append(line).append(System.lineSeparator());
        }
        input.close();
        return info.toString();
    }
}
