
# 配置文件属性详解


```
{
        "admin": {
            "user_info": "/yokead/order/config/admin/user.json",  
            "temp_dir": "/yokead/order/config/admin/user_temp",   
            "default_user": { (系统默认账号密码，如需修改请在此修改并重启系统)
                "name": "admin",
                "pwd": "123456"
            },
            "default_ip": "127.0.0.1", (域名解析和调整默认ip)
            "auto_load_config": true, (是否自动刷新配置文件)
            "page_template": "/yokead/order/config/admin/page_template",
            "port": 1126  (后台系统的端口)
        },
        "dbConfig": {
            "config": "file:/yokead/order/config/db_init.properties"  
        },
        "web": {
            "copy_api_url": "http://127.0.0.1:1126/api/copylist.json", (前台获取订单使用的文案接口地址)
            "port": 1127  (前台系统的端口)
        },
        "nginx": {
            "config_path": "/etc/nginx/conf.d/ad",
            "default_config_template": "/yokead/order/config/admin/nginx_default.conf",
    (6.4.3)
            "static_path": "/yokead/order/nginx-static/html",
            "log_path": "/var/log/nginx/log",  (默认配置的nginx 日志路径)
            "exclude_resolve_path": [
                "/yokead/order/nginx-static/error"   (系统解析nginx配置文件排除此目录)
            ]
        },
        "qiniu": {
            "config_path": "/yokead/order/config/admin/qiniu_info.json" 
        },
        "dnspod": {
            "config_path": "/yokead/order/config/admin/dnspod.json",  
            "domain_list": "/yokead/order/config/admin/domain_list.json", 
            "log_path": "/yokead/order/config/admin/domain" 
        },
        "order": {
            "config_path": "/yokead/order/config/order/customer",  
            "type_path": "/yokead/order/config/order/ordertype",  
            "oper_url_prefix": "http://order.web.cangyou100.com/oper/"  (后台创建的临时运营人员登录地址)
        },
        "shield": {
            "config_path": "/yokead/order/config/shield",  (配置对应配置目录)
            "clear_cache": "http://127.0.0.1:1127/shield/opt",   (屏蔽相关操作接口)
            "nginx_proxy_pass": "http://127.0.0.1:1127/shield/"  (屏蔽接口的动态代理地址)
        }
    }

```
