
SET FOREIGN_KEY_CHECKS=0;

CREATE TABLE `%s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `sum` int(11) DEFAULT NULL,
  `isDelete` int(11) DEFAULT 0,
  `createTime` int(11) DEFAULT 0,
  `modifyTime` int(11) DEFAULT 0,
  `mark` varchar(255) DEFAULT NULL,
  `product` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `gid` varchar(255) DEFAULT NULL,
  `uga` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
