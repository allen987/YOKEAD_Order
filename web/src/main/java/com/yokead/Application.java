package com.yokead;

import com.yokead.common.YokeadApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by jiangzeyin on 2017/5/9.
 */
@SpringBootApplication
@ServletComponentScan
@ComponentScan({"com.yokead"})
public class Application {
    /**
     * 启动执行
     *
     * @param args args
     */
    public static void main(String[] args) throws Exception {
        YokeadApplication.createBuilder("web", Application.class).run(args);
    }
}
