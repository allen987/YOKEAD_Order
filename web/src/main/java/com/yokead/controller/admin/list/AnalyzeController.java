package com.yokead.controller.admin.list;

import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.OrderListBaseController;
import com.yokead.service.AnalyzeService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by jiangzeyin on 2018/5/16.
 */
@Controller
public class AnalyzeController extends OrderListBaseController {


    @Resource
    private AnalyzeService analyzeService;

    @RequestMapping(value = "analyze.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String list() {
        return "admin/list/analyze";
    }


    @RequestMapping(value = "analyze_data.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String analyze_data(String date, String product) {
        if (StringUtil.isEmpty(product))
            return JsonMessage.getString(405, "请选择产品");
        String tableName = getTableName(userName, product);
        if (StringUtil.isEmpty(tableName))
            return JsonMessage.getString(405, "产品信息获取失败");
        Object dateInt = analyzeService.parseDate(date);
        if (dateInt instanceof String) {
            return (String) dateInt;
        }
        int[] date_ = (int[]) dateInt;
        JSONObject jsonObject = analyzeService.getData(tableName, date_);
        return JsonMessage.getString(200, "", jsonObject);
    }


}
