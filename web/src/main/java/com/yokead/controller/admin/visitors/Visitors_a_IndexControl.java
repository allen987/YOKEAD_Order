package com.yokead.controller.admin.visitors;

import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.OrderBaseControl;
import com.yokead.service.VisitorsService;
import com.yokead.service.order.ProductService;
import com.yokead.system.log.LogType;
import com.yokead.system.log.SystemLog;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Iterator;
import java.util.UUID;

/**
 * Created by jiangzeyin on 2017/9/22.
 */
@Controller
@RequestMapping("visitors_a")
public class Visitors_a_IndexControl extends OrderBaseControl {
    @Resource
    private VisitorsService visitorsService;

    @Resource
    private ProductService productService;

    @RequestMapping(value = "index.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String index() {
        try {
            setAttribute("array", visitorsService.getUserVisitorsJson(userName));
        } catch (IOException e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("获取异常", e);
        }
        StringBuffer url = getRequest().getRequestURL();
        String uri = getRequest().getRequestURI();
        String tempContextUrl = url.delete(url.length() - uri.length(), url.length()).toString();
        tempContextUrl += "/visitors/index.html?name=" + userName;
        setAttribute("url", tempContextUrl);
        return "admin/visitors/visitors_index";
    }

    @RequestMapping(value = "add.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String add(String id) {
        try {
            setAttribute("array", productService.getUserAll(userName));
        } catch (IOException e) {
            SystemLog.LOG(LogType.CONTROL_ERROR).error("获取异常", e);
        }
        setAttribute("item", visitorsService.getInfoById(userName, id));
        return "admin/visitors/visitors_add";
    }

    @RequestMapping(value = "save.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String save(String id, String name, String pwd, String mark, String plist) {
        synchronized (Visitors_a_IndexControl.class) {
            if (StringUtil.isEmpty(name, 4, 20))
                return JsonMessage.getString(100, "登录名长度4-20");
            if (name.contains("@"))
                return JsonMessage.getString(100, "登录名不能包含@");
            // 添加验证登录名
            if (StringUtil.isEmpty(id)) {
                if (!visitorsService.checkUserName(userName, name))
                    return JsonMessage.getString(100, "登录名已经存在");
            }
            // 添加必须验证登录
            if (StringUtil.isEmpty(id)) {
                if (StringUtil.isEmpty(pwd, 4, 20))
                    return JsonMessage.getString(100, "密码长度4-20");
            } else {
                // 修改密码验证
                if (!StringUtil.isEmpty(pwd)) {
                    if (StringUtil.isEmpty(pwd, 4, 20))
                        return JsonMessage.getString(100, "密码长度4-20");
                }
            }
            if (StringUtil.isEmpty(mark))
                return JsonMessage.getString(100, "备注不能为空");
            if (StringUtil.isEmpty(plist))
                return JsonMessage.getString(100, "请选择产品");
            String[] plist_s = StringUtil.stringToArray(plist, ",");
            if (plist_s == null || plist_s.length <= 0)
                return JsonMessage.getString(100, "请选择产品:-1");
            JSONArray jsonArray;
            try {
                jsonArray = visitorsService.getUserVisitorsJson(userName);
                if (jsonArray == null)
                    jsonArray = new JSONArray();
            } catch (IOException e) {
                SystemLog.LOG(LogType.CONTROL_ERROR).error("获取异常", e);
                return JsonMessage.getString(300, "系統处理异常请稍后再试");
            }
            boolean del = getParameter("auth_del", "").equalsIgnoreCase("on");
            boolean analyze = getParameter("auth_analyze", "").equalsIgnoreCase("on");
            JSONObject auth = new JSONObject();
            auth.put("del", del);
            auth.put("analyze", analyze);
            boolean u = false;
            if (StringUtil.isEmpty(id)) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("id", UUID.randomUUID().toString());
                jsonObject.put("name", name);
                jsonObject.put("pwd", pwd);
                jsonObject.put("mark", mark);
                jsonObject.put("auth", auth);
                jsonObject.put("list", plist_s);
                jsonArray.add(jsonObject);
                u = true;
            } else {
                for (Object aJsonArray : jsonArray) {
                    JSONObject jsonObject = (JSONObject) aJsonArray;
                    String id_ = jsonObject.getString("id");
                    if (id.equals(id_)) {
                        if (!StringUtil.isEmpty(pwd))
                            jsonObject.put("pwd", pwd);
                        jsonObject.put("mark", mark);
                        jsonObject.put("list", plist_s);
                        jsonObject.put("auth", auth);
                        u = true;
                    }
                }
            }
            if (u) {
                try {
                    visitorsService.saveUserVisitorsJson(userName, jsonArray);
                } catch (IOException e) {
                    SystemLog.LOG(LogType.CONTROL_ERROR).error("获取异常", e);
                    return JsonMessage.getString(300, "系統处理异常请稍后再试:-2");
                }
                return JsonMessage.getString(200, "ok");
            }
            return JsonMessage.getString(400, "信息不正确");
        }
    }

    @RequestMapping(value = "del.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String save(String id) {
        synchronized (Visitors_a_IndexControl.class) {
            JSONArray jsonArray;
            try {
                jsonArray = visitorsService.getUserVisitorsJson(userName);
            } catch (IOException e) {
                SystemLog.LOG(LogType.CONTROL_ERROR).error("获取异常", e);
                return JsonMessage.getString(100, "系統处理异常请稍后再试");
            }
            if (jsonArray == null)
                return JsonMessage.getString(100, "不能操作");
            Iterator<Object> iterator = jsonArray.iterator();
            boolean u = false;
            while (iterator.hasNext()) {
                JSONObject jsonObject = (JSONObject) iterator.next();
                String id_ = jsonObject.getString("id");
                if (id_.equals(id)) {
                    iterator.remove();
                    u = true;
                    break;
                }
            }
            if (u) {
                try {
                    visitorsService.saveUserVisitorsJson(userName, jsonArray);
                } catch (IOException e) {
                    SystemLog.LOG(LogType.CONTROL_ERROR).error("获取异常", e);
                    return JsonMessage.getString(100, "系統处理异常请稍后再试:-2");
                }
                return JsonMessage.getString(200, "ok");
            }
            return JsonMessage.getString(100, "没有对应信息");
        }
    }

}
