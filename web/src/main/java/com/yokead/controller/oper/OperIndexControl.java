package com.yokead.controller.oper;

import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import com.alibaba.fastjson.JSONObject;
import com.yokead.common.OrderBaseControl;
import com.yokead.common.interceptor.NotLogin;
import com.yokead.controller.visitors.VisitorsIndexControl;
import com.yokead.service.order.OrderService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created by jiangzeyin on 2017/6/22.
 */
@Controller
@RequestMapping("oper")
public class OperIndexControl extends OrderBaseControl {

    @Resource
    private OrderService orderService;

    /**
     * @return
     */
    @RequestMapping(value = "{key}", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    @NotLogin
    public String list(@PathVariable String key, String name) throws IOException {
        String sKey = StringUtil.convertNULL(getSessionAttribute("Oper_User"));
        if (StringUtil.isEmpty(sKey))
            return "redirect:/oper/login.html?key=" + key + "&name=" + name;
        String oS = String.format("%s=%s", key, name);
        String[] tempS = StringUtil.stringToArray(sKey, "@");
        if (tempS == null || tempS.length != 2)
            return "redirect:/oper/login.html?key=" + key + "&name=" + name;
        if (!oS.equals(tempS[0]))
            return "redirect:/oper/login.html?key=" + key + "&name=" + name;
        name = convertFilePath(name);
        JSONObject jsonOrder = orderService.getProductOperColumn(name, tempS[1]);
        if (jsonOrder == null)
            return "redirect:/oper/login.html?key=" + key + "&name=" + name;
        setAttribute("columns", jsonOrder.getJSONArray("columns"));
        //request.setAttribute("name", name);
        //request.setAttribute("product", productName);
        return "oper/index";
    }

    @RequestMapping(value = "list_data.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @NotLogin
    public String listData(String name, String pageNo, String pageSize, String type) {
        name = convertFilePath(name);
        int pageNoInt = StringUtil.parseInt(pageNo, 1);
        int pageSizeInt = StringUtil.parseInt(pageSize, 10);
        String sKey = StringUtil.convertNULL(getSessionAttribute("Oper_User"));
        if (StringUtil.isEmpty(sKey))
            return JsonMessage.getString(100, "获取失败");
        String[] tempS = StringUtil.stringToArray(sKey, "@");
        if (tempS == null || tempS.length != 2)
            return JsonMessage.getString(100, "获取失败，错误");
        return orderService.doSelect(name, tempS[1], pageNoInt, pageSizeInt, type, 0, null, null, VisitorsIndexControl.coverInfoHashMap);
    }

    @RequestMapping(value = "ad_type.json", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @NotLogin
    public String getAd(String name) {
        name = convertFilePath(name);
        String sKey = StringUtil.convertNULL(getSessionAttribute("Oper_User"));
        if (StringUtil.isEmpty(sKey))
            return JsonMessage.getString(100, "获取失败");
        String[] tempS = StringUtil.stringToArray(sKey, "@");
        if (tempS == null || tempS.length != 2)
            return JsonMessage.getString(100, "获取失败，错误");
        return orderService.getAdType(name, tempS[1]);
    }
}
