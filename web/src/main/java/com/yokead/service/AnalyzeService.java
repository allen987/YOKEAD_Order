package com.yokead.service;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.JsonMessage;
import cn.simplifydb.database.base.BaseRead;
import cn.simplifydb.database.run.read.Select;
import com.alibaba.fastjson.JSONObject;
import com.yokead.system.log.SystemLog;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * @author jiangzeyin
 * @date 2018/5/16
 */
@Service
public class AnalyzeService {

    public Object parseDate(String date) {
        int[] date_ = new int[2];
        try {
            if (!StringUtil.isEmpty(date)) {
                String[] dates = StringUtil.stringToArray(date, "_");
                if (dates == null || dates.length < 2) {
                    return JsonMessage.getString(103, "时间选择不正确");
                }
                date_[0] = (int) (DateUtil.parse(dates[0], "yyyy-MM-dd").getTime() / 1000L);
                date_[1] = (int) ((int) (DateUtil.parse(dates[1], "yyyy-MM-dd").getTime() / 1000L) + TimeUnit.HOURS.toSeconds(24));
            } else {
                String toDate = new DateTime().toString("yyyy-MM-dd");
                date_[0] = (int) (DateUtil.parse(toDate, "yyyy-MM-dd").getTime() / 1000L);
                date_[1] = (int) ((int) (DateUtil.parse(toDate, "yyyy-MM-dd").getTime() / 1000L) + TimeUnit.HOURS.toSeconds(24));
            }
            return date_;
        } catch (Exception e) {
            SystemLog.ERROR().error("时间格式不正确:" + date, e);
            return JsonMessage.getString(103, "时间格式不正确");
        }
    }

    public JSONObject getData(String tableName, int[] date) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("order_number", getData(tableName, date, "order_number"));
        jsonObject.put("order_sum", getData(tableName, date, "order_sum"));
        jsonObject.put("ip_count", getData(tableName, date, "ip_count"));
        jsonObject.put("name_count", getData(tableName, date, "name_count"));
        jsonObject.put("phone_count", getData(tableName, date, "phone_count"));
        return jsonObject;
    }

    private int getData(String tableName, int[] date, String way) {
        String column;
        if ("order_number".equals(way)) {
            column = "count(*)";
        } else if ("order_sum".equals(way)) {
            column = "SUM(sum)";
        } else if ("ip_count".equals(way)) {
            column = "count(DISTINCT ip)";
        } else if ("name_count".equals(way)) {
            column = "count(DISTINCT name)";
        } else if ("phone_count".equals(way)) {
            column = "count(DISTINCT phone)";
        } else {
            return 0;
        }
        String sql = String.format("select %s as con from %s where createTime>=%d and createTime<=%d", column, tableName, date[0], date[1]);
        Select<?> select = new Select<>();
        select.setTag("core");
        select.setSql(sql);
        select.setResultType(BaseRead.Result.Integer);
        return StringUtil.parseInt((Object) select.run());
    }
}
