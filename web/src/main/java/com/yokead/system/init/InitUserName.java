package com.yokead.system.init;

import cn.jiangzeyin.StringUtil;
import cn.jiangzeyin.common.PreLoadClass;
import cn.jiangzeyin.common.PreLoadMethod;
import com.yokead.common.interceptor.LoginInterceptor;
import com.yokead.system.log.aop.WebLog;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by jiangzeyin on 2018/5/9.
 */
@PreLoadClass
public class InitUserName {
    @PreLoadMethod
    private static void init() {
        WebLog.setGetUserName(InitUserName::getName);
    }

    public static String getName() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String name = (String) request.getSession().getAttribute(LoginInterceptor.SESSION_NAME);
        String vUser = (String) request.getSession().getAttribute("Visitors_User");
        String oUser = (String) request.getSession().getAttribute("Oper_User");
        return String.format("%s|%s|%s", StringUtil.convertNULL(name), StringUtil.convertNULL(vUser), StringUtil.convertNULL(oUser));
    }
}
